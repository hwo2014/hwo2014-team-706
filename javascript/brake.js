/* ********************************************
 * Brake factor evaluation
 *
 * owns stats and speed / angle limits
 * guesses the unknown
 */
module.exports = function(logger) {
  // used to compute break distance
  this.drag = undefined;
  this.decel = K.DECEL;
  // velocity and travelled space
  // at sample start
  this.start = {
    v: null
  , s: null
  };
  // velocity to reach in some piece
  this.target = {
    v: null
  , piece: null
  };
  // holds ratios of dv / ds for breaks
  // outside expected range
  this.sample = {
    r: []
  , max: 0
  , min: Number.MAX_VALUE
  };
  var _dragSample = [];

  this.addDragSample = function(speed) {
    if (speed > 0) {
      if (speed <= _.last(_dragSample))
        _dragSample.shift();
      _dragSample.push(speed);
    }
  };
  this.canEvalDrag = function() {
    return _dragSample.length > 1;
  }
  // eval drag, return mass
  this.evalDrag = function(throttle) {
    K.DRAG = this.drag = (_dragSample[0] - (_dragSample[1] - _dragSample[0]) ) / Math.pow(_dragSample[0], 2) * throttle;
    return (K.CAR_MASS = (1 / ( Math.log( (_dragSample[1] - (throttle/this.drag)) / (_dragSample[0] - (throttle/this.drag)) ) / (-this.drag) )));
  };
  // tells brake distance to reach target speed 
  // given current one
  this.distance = function(curSpeed, targetSpeed) {
    if (curSpeed > targetSpeed) {
      return Math.pow(curSpeed - targetSpeed, 1) / (2 * this.decel * K.G);
//       return Math.pow(curSpeed - targetSpeed, 2) / (2 * this.drag * K.G);
    } else {
      return 0;
    }
  };
  // tells if a sample is in progress
  this.isSampling = function() {
    return this.target.piece != null;
  };
  // start a new sample
  this.startSample = function(start, target) {
    this.start = start;
    this.target = target;
  };
  // reset sample
  this.reset = function() {
    this.start.v = null;
    this.start.s = null;
    this.target.v = null;
    this.target.piece = null;
  };
  // store sample and update deceleration factor
  this.update = function(newSpeed, distance) {
    if (distance > this.start.s) {
      var r = (this.start.v - newSpeed) / (distance - this.start.s);
      this.sample.r.push(r);
      if (r > this.sample.max)
        this.sample.max = r;
      if (r < this.sample.min)
        this.sample.min = r;
      var ratios = _.filter(this.sample.r, function(r) {
        return r < this.sample.max &&
               r > this.sample.min;
      }, this);
      if (ratios.length) {
        this.decel = (_.sum(ratios) / ratios.length) / (2 * K.G);
        logger.grip('Decel', this.decel);
      }
    }
    this.reset();
  };
};