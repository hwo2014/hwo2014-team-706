var Track = require('./track.js');
var Car = require('./car.js');
var Opponent = require('./opponent.js');

/* ********************************************
 * Race
 *
 * Holds track and cars
 */
module.exports = function(logger) {
  this.car       = null;
  this.track     = null;
  this.opponents = {};
  this.active    = false;
  this.qualifications = true;
  this.time      = null;

  /* ********************************************
   * LAPS
   */

  var _laps = {
        total: undefined
      , toGo: undefined
      , done: 0
      }
    , _timeLimit = undefined
    ;
  // one lap done
  this.newLap = function() {
    --_laps.toGo;
    ++_laps.done;
  };
  // is this last lap ?
  this.lastLap = function() {
    if (_laps.toGo === undefined) {
      return this.time + this.car.lapTime() >= _timeLimit;
    } else {
      return _laps.toGo <= 1;
    }
  };

  /* ********************************************
   * INIT
   * my car, track and opponents
   */

  var _myCarData = null
    ;
  this.setCarData = function(myCarData) {
    _myCarData = myCarData;
  };

  this.setTrackData = function(gameInitData) {
    if (this.track) {
      _.invoke(this.opponents, 'reset');
    } else {
      // track
      this.track = new Track(logger, gameInitData, this);
      // car
      this.car = new Car(logger, _myCarData, this);
      delete(_myCarData), _myCarData = undefined;
      // opponents and self
      _.each(gameInitData.data.race.cars, function(carData) {
        if (carData.id.color != this.car.color) {
          this.opponents[carData.id.color] = new Opponent(logger, carData, this);
        } else {
          this.car.length = carData.dimensions.length;
        }
      }, this);
      this.opponents[this.car.color] = this.car;

      logger.info(this.car.name, "'s car has color", this.car.color);
//       logger.info('Track', gameInitData);
//       logger.info('Opponents', this.opponents);
    }
    _laps.total = _laps.toGo = 
      (gameInitData.data.race.raceSession.laps || null);
    _timeLimit = gameInitData.data.race.raceSession.durationMs;
  };

  this.crash = function(carColor) {
    this.opponents[carColor].crash();
  };

  this.spawn = function(carColor) {
    this.opponents[carColor].spawn();
  };

  this.collisionTarget = function(laneIdx) {
    var feasible = _.find(this.opponents, function(o) {
      return o.color != this.car.color
          && o.closeTo(this.car, laneIdx);
    }, this);

    return feasible;
  };

  this.killTarget = function(laneIdx) {
    var feasible = _.find(this.opponents, function(o) {
      return o.color != this.car.color
          && o.killTarget(this.car, laneIdx);
    }, this);

    return feasible;
  };

  this.opponentsNearMe = function() {
    var rt = false;
    _.each(this.opponents, function(o) {
      if (o.color !== this.car.color) {
        _.each([-1,0,1], function(d) {
          var otherPiece = ((o.position.piecePosition.pieceIndex + d + this.track.pieces) % this.track.pieces);
          if (otherPiece == this.car.position.piecePosition.pieceIndex &&
              o.lane == this.car.lane)
            rt = true;
        }, this);
      }
    }, this);
    return rt;
  };

  this.opponentsNear = function(a) {
    var rt = false;
    _.each(this.opponents, function(o) {
      if (o.color !== a.color) {
        if (o.position.piecePosition.pieceIndex == a.position.piecePosition.pieceIndex &&
            o.lane == a.lane &&
            (between(o.position.piecePosition.inPieceDistance - o.length - a.position.piecePosition.inPieceDistance, 0, 10) ||
             between(a.position.piecePosition.inPieceDistance - a.length - o.position.piecePosition.inPieceDistance, 0, 10)))
          rt = true;
      }
    }, this);
    return rt;
  };
  
  this.logStats = function() {
//     this.track.logStats();
    logger.info('Opponents');
    _.each(this.opponents, function(o) {
      if (o.color !== this.car.color)
        logger.info(o);
    }, this);
  };
};