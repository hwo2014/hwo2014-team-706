var PieceLimits = require('./piece-limits.js');
var TrackBests  = require('./track-bests.js');

/* ********************************************
 * Track
 *
 * contains lanes and pieces
 * provides switch evaluation heuristic
 */

module.exports = function(logger, gameInitData, race) {
  var _pieces = []
    , _lanes  = {}
    , _bends  = {}
    , _paths  = []
    , _straights = []
    ;

  this.pieces = 0;
  this.lanes  = 0;
  this.name   = gameInitData.data.race.track.id;

  /* ********************************************
   * INIT
   * 
   * lanes
   * pieces
   * speed limits for each piece
   */

  // init lanes with idx as hash key
  _.each(gameInitData.data.race.track.lanes, function(lane) {
    _lanes[lane.index] = lane;
    ++this.lanes;
  }, this);
  // init limits
  var _pieceCount = {};
  _.each(gameInitData.data.race.track.pieces, function(piece) {
    // piece type stats
    _pieceCount[piece.radius || 0] = (_pieceCount[piece.radius || 0] || 0) + 1;
    // extend piece with limits and push
    _pieces.push(
      _.extend({ index: this.pieces }, 
               piece, 
               new PieceLimits(piece, _lanes)
              )
    );
    ++this.pieces;
  }, this);

  this.bests  = new TrackBests(this);
  this.bests.load();
  
  /* ********************************************
   * getters
   */

  // --------------------------------------------
  // get lane by idx
  this.lane = function(idx) {
    return _lanes[idx];
  };
  // --------------------------------------------
  // get piece by idx
  this.piece = function(idx) {
    return _pieces[idx];
  };
  // --------------------------------------------
  // bend radius in a particular lane
  this.bendRadius = function(piece, lane) {
    return piece.radius - lane.distanceFromCenter * sign(piece.angle);
  };
  // --------------------------------------------
  // piece length if walked through a particular lane
  this.pieceLength = function(pieceIdx, fromLaneIdx, toLaneIdx) {
    var piece = _pieces[pieceIdx]
      , fromLane = _lanes[fromLaneIdx]
      , toLane = _lanes[toLaneIdx]
      ;

    if (fromLaneIdx == toLaneIdx) {
      return (piece.radius
        ? (Math.abs(piece.angle) / 180) * Math.PI * this.bendRadius(piece, fromLane)
        : piece.length
      );
    } else if (piece.length) {
      return Math.sqrt(
        Math.pow(piece.length, 2) + // dx
        Math.pow(toLane.distanceFromCenter - fromLane.distanceFromCenter, 2)  // dy
      );
    } else {
      var a = piece.angle * Math.PI / 180
        , toRadius = this.bendRadius(piece, toLane)
        ;
      return Math.sqrt(
        Math.pow(toRadius * Math.cos(a) - this.bendRadius(piece, fromLane), 2) + // dx
        Math.pow(toRadius * Math.sin(a), 2)  // dy
      );
    }
  };

  // --------------------------------------------
  // get distance from start given a switch policy
  this.travelledDistance = function(pieceIdx, inPieceDistance, switchPolicy) {
    var i = 0
      , s = 0
      , laneIdx = switchPolicy.lanes[0]
      ;
    while(i < pieceIdx) {
      if (switchPolicy.pieces[s] === i) {
        inPieceDistance += this.pieceLength(i, laneIdx, switchPolicy.lanes[s]);
        laneIdx = switchPolicy.lanes[s];
        ++s;
      } else {
        inPieceDistance += this.pieceLength(i, laneIdx, laneIdx);
      }
      ++i;
    }
    return inPieceDistance;
  };
  // --------------------------------------------
  // get distance from start on a given lane
  this.travelledDistanceInLane = function(pieceIdx, inPieceDistance, laneIdx) {
    for(i = 0; i < pieceIdx; ++i) {
      inPieceDistance += this.pieceLength(pieceIdx, laneIdx, laneIdx);
    }
    return inPieceDistance;
  };

  /* ********************************************
   * speed limit adjust
   */

  this.adjustLimitFactors = function(f) {
    _.each(_pieces, function(p) {
      if (p.radius)
        _.each(_.keys(_lanes), function(l) {
          p.limit[l].raise *= f;
          p.limit[l].lower *= f;
        });
    });
  };

  this.resetLowerLimitFactors = function() {
    _.each(_pieces, function(p) {
      if (p.radius)
        _.each(_.keys(_lanes), function(l) {
          p.limit[l].lower = K.BEND_LIMITS[p.radius].lower;
        });
    });
  };

  // adjust limits of bends similar to some model
  // that experienced no crashes or successes
  var _adjustBendLimits = function(pieceModel, factor, laneIdx, speed, prop) {
    _.each(_pieces, function(piece) {
      if (piece !== pieceModel && 
          !piece.hasExperience(laneIdx) &&
          piece.radius == pieceModel.radius && 
          closeTo(piece.speedLimit(laneIdx), pieceModel.speedLimit(laneIdx)))
      {
        if (speed !== null) {
          piece.setSpeedLimit(laneIdx, speed);
        } else {
          piece.changeSpeedLimit(laneIdx, factor);
        }

        ++ piece.stats[laneIdx][prop];
        logger.grip(piece);
      }
    });
  };

  // --------------------------------------------
  // raise limits of some bend
  // if no crashes ever occured
  // share experience with similar bends
  this.raiseLimits = function(pieceModel, laneIdx, speed) {
    var factor = 1 + pieceModel.limit[laneIdx].raise;
    if (factor > 1) {
      // raise limits just of bends
      if (pieceModel.radius) {
        logger.grip('Raising limits in lane', laneIdx);
        // raise self just if never crashed
        if (!pieceModel.crashed(laneIdx)) {
          speed = Math.max(speed, pieceModel.speedLimit(laneIdx) * factor);
          _adjustBendLimits(pieceModel, factor, laneIdx, speed, 'successes');
          pieceModel.setSpeedLimit(laneIdx, speed);
          logger.grip(pieceModel);
        } else {
          _adjustBendLimits(pieceModel, factor, laneIdx, speed, 'successes');
        }
      }
    }
  };
  
  // --------------------------------------------
  // lower limits of some piece
  // if it's a bend, share experience
  this.lowerLimits = function(pieceModel, laneIdx, speed) {
    var factor = 1 - pieceModel.limit[laneIdx].lower;
    logger.grip('Lowering limits in lane', laneIdx);
    speed = Math.min(speed || Number.MAX_VALUE, pieceModel.speedLimit(laneIdx)) * factor;
    if (pieceModel.radius)
      _adjustBendLimits(pieceModel, factor, laneIdx, speed, 'crashes');
    pieceModel.setSpeedLimit(laneIdx, speed);
    logger.grip(pieceModel);
  };

  /* ********************************************
   * BEST PATH static evaluation
   */
  {
    // build a path tree starting from a piece and lane
    // cumulate len up to next switch
    var buildPath = _.bind(function(pieceIdx, fromLaneIdx, toLaneIdx, loop) {
      var rt = {
            lane: toLaneIdx
          , piece: pieceIdx // 0, or a switch
          , length: this.pieceLength(pieceIdx, fromLaneIdx, toLaneIdx)
          }
        ;
      if (loop || pieceIdx) {
        // stop when start, or self, is reached again
        // or a switch is found
        var nextPieceIdx = (pieceIdx + 1) % this.pieces
          , nextPiece = this.piece(nextPieceIdx)
          ;
        while(nextPieceIdx &&
              nextPieceIdx != pieceIdx &&
              !nextPiece.switch)
        {
          rt.length += this.pieceLength(nextPieceIdx, toLaneIdx, toLaneIdx);
          nextPieceIdx = (nextPieceIdx + 1) % this.pieces;
          nextPiece = this.piece(nextPieceIdx);
        }
        // if next piece is a switch, build
        // alternative paths
        if (nextPiece.switch) {
          rt.alternatives = buildAlternativePaths(nextPieceIdx, toLaneIdx, false);
        }
      }
      return rt;
    }, this);

    // build all paths starting from a piece and lane
    // adds adjacent
    var buildAlternativePaths = _.bind(function(pieceIdx, laneIdx, loop) {
      var rt = [];

      rt.push(buildPath(pieceIdx, laneIdx, laneIdx, loop));
      if (this.piece(pieceIdx).switch) {
        if (laneIdx > 0)
          rt.push(buildPath(pieceIdx, laneIdx, laneIdx - 1, loop));
        if (laneIdx + 1 < this.lanes)
          rt.push(buildPath(pieceIdx, laneIdx, laneIdx + 1, loop));
      }

      return rt;
    }, this);

    // recursively builds every single full path
    // computing total length and number of switches
    var computePathLengths = function(path) {
      var info = {
            lanes: [path.lane]
          , pieces: [path.piece]  // first plus any switch
          //, chunks: [path.length]
          , length: path.length
          , switches: 0
          }
        , rt = [];
      if (path.alternatives) {
        _.each(path.alternatives, function(node) {
          _.each(computePathLengths(node), function(sub) {
            var subInfo = {
                  lanes: info.lanes.concat(sub.lanes)
                , pieces: info.pieces.concat(sub.pieces)
                //, chunks: info.chunks.concat(sub.chunks)
                , length: info.length + sub.length
                , switches: (path.lane == sub.lanes[0] ? 0 : 1) + sub.switches
                };
            rt.push(subInfo);
          });
        });
      } else {
        rt.push(info);
      }
      return rt;
    };

    var trees = []
      , alternatives = []
      ;
    // **************************************************
    // compute path tree
    _.each(_lanes, function(lane) {
      trees.push(buildPath(0, lane.index, lane.index, true));
    }, this);

    // flatten tree multiplying alternatives
    _.each(trees, function(path) {
      alternatives = alternatives.concat(computePathLengths(path));
    });
    logger.path("Alternative paths:", alternatives.length);

    // sort alternatives by length and switches
    var lengthGroups = _.groupBy(alternatives, 'length');
    var lengths = _.sortBy(_.keys(lengthGroups), function(x) {
      return x*1;
    });
    _.each(lengths, function(length) {
      _paths.push.apply(_paths, _.sortBy(lengthGroups[length], 'switches'));
    }, this);
    logger.path("Path lengths", lengths);
    //logger.path("All paths:", _paths);

    // **************************************************
    // compute straights

    var pieceIdx = 0
      , piece = this.piece(0)
    do {
      if (piece.length) {
        var straight = {
          startPiece: pieceIdx
        , length: piece.length
        , pieces: [pieceIdx]
        , chunks: [piece.length]
        };
        piece = this.piece(++pieceIdx);
        while(pieceIdx < this.pieces && piece.length) {
          straight.length += piece.length;
          straight.pieces.push(pieceIdx);
          straight.chunks.push(piece.length);
          piece = this.piece(++pieceIdx);
        }
        straight.endPiece = pieceIdx - 1;
        _straights.push(straight);
      }
      piece = this.piece(++pieceIdx);
    } while(pieceIdx < this.pieces);
    // merge first to last if same straight
    var firstStraight = _.first(_straights)
      , lastStraight = _.last(_straights)
      ;
    if (lastStraight.endPiece == this.pieces - 1 && 
        firstStraight.startPiece == 0) 
    {
      firstStraight.startPiece = lastStraight.startPiece;
      firstStraight.length += lastStraight.length;
      firstStraight.pieces = lastStraight.pieces.concat(firstStraight.pieces);
      firstStraight.chunks = lastStraight.chunks.concat(firstStraight.chunks);
      _straights.pop();
    }
    _straights = _.sortBy(_straights, 'length').reverse();
    logger.path('Straights:', _straights);
  }

  // --------------------------------------------
  // get the switch index in the piece array of
  // polices, i.e. the index of the next switch piece
  // relative to the given piece
  this.switchPieceIndex = function(pieceIdx, policy) {
    // any policy is fine, all switches are the same
    var pieces = (policy || _paths[0]).pieces
      , rt = 1
      ;
    while(rt < pieces.length && pieceIdx >= pieces[rt])
      ++rt;
    return Math.max(1, rt % pieces.length);
  };
  // --------------------------------------------
  // get lane we'll have in some piece in some policy
  this.switchLane = function(pieceIndex, policy) {
    var idx = this.switchPieceIndex(pieceIndex, policy) - 1;
    if (idx == 0) idx = policy.pieces.length - 1;
    return policy.lanes[idx];
  };
  // --------------------------------------------
  // get switch policy at start piece
  this.switchStartPolicy = function(laneIdx) {
    // best policy overall looking just forward
    return _.find(_paths, function(path) {
      return  path.lanes[0] == laneIdx;
    });
  };
  // --------------------------------------------
  // find a policy matching the given sequence
  this.matchPolicy = function(pieces, lanes) {
    var mp = [], ml = []
      , i = 0, j = 0
      , idx = 0
      , pp = _paths[0].pieces
      ;
//       console.log(JSON.stringify(pieces), JSON.stringify(lanes));
//       console.log(JSON.stringify(pp));
    // get first index in policy
    if (_.first(pieces) > _.last(pieces)) {
      // roll back
      idx = pp.length-1;
      if (pp[idx] == 0)
        --idx;
      while(pieces[i] <= pp[idx])
        --idx;
    } 
    // roll fwd
    while(pieces[i] > pp[idx])
      ++idx;
//     console.log(pieces[i] , '->', idx, 'at', i);
    // build policy segment
    j = idx = Math.min(idx, pp.length-1);
    for(loop=true;loop;) {
      // discard useless
      while(i < pieces.length && pieces[i] < pp[j]) {
        ++i;
      }
      if (i < pieces.length) {
        if (j >= pp.length-1) {
          mp.push(pp[j]);
          ml.push(lanes[i]);
          loop = false;
        } else if (i < pieces.length) {
          mp.push(pieces[i]);
          ml.push(lanes[i]);
          if (++j >= pp.length)
            loop = false;
        }
      } else {
        loop = false;
      }
    }
    // search
//     console.log(JSON.stringify(mp), JSON.stringify(ml));
    return _.find(_paths, function(path) {
      for(i = 0; i < mp.length; ++i) {
        j = (idx + i) % pp.length;
        if (mp[i] != path.pieces[j] || ml[i] != path.lanes[j])
          return false;
      }
      return true;
    });
  }
  // --------------------------------------------
  // get switch policy at a piece inside the track
  this.switchRunningPolicy = function(curLaneIdx, targetLaneIdx, switchIndex) {
    // new policy matches target lane index in next path portion
    // and current lane index in current path portion
    // path portion index is always behind by 1
    return _.find(_paths, function(path) {
      var prevIndex = (switchIndex - 1) || (path.lanes.length - 1);
      return  path.lanes[prevIndex] == curLaneIdx &&
              path.lanes[switchIndex] == targetLaneIdx;
    });
  };
  // --------------------------------------------
  // tells if two switch indexes are close
  this.closeSwitches = function(a,b) {
    if (a == b)
      return true;
    else if (a > b)
      return (a - b <= 1) || 
             (a == _paths[0].lanes.length - 1 && b <= 1) ||
             (a == _paths[0].lanes.length - 2 && b == 0)
             ;
    else
      return (b - a <= 1) || 
             (b == _paths[0].lanes.length - 1 && a <= 1) ||
             (b == _paths[0].lanes.length - 2 && a == 0)
             ;
  }
  /* ********************************************
   * STRAIGHTS
   */
  
  // --------------------------------------------
  // get info on next bend when in a straight
  this.bendAfterStraight = function(pieceIdx) {
    var straightPiece = -1
      , straight = _.find(_straights, function(s) {
          straightPiece = s.pieces.indexOf(pieceIdx);
          return (straightPiece >= 0);
        })
      , bend
      ;
    if (straight) {
      var bendIdx = (_.last(straight.pieces) + 1) % this.pieces;
      return {
        pieceIndex: bendIdx
      , piece: this.piece(bendIdx)
      , straight: straight
      , pieceIndexInStraight: straightPiece
      };
    } else {
      return null;
    }
  };
  // --------------------------------------------
  // get distance to next bend when in a straight
  // using info returned from bendAfterStraight
  this.distanceToBendAfterStraight = function(bendAfterStraight, inPieceDistance) {
    var straight = bendAfterStraight.straight
      , straightPiece = bendAfterStraight.pieceIndexInStraight
      , distance = straight.chunks[straightPiece] - inPieceDistance
      ;

      while(++straightPiece < straight.pieces.length)
        distance += straight.chunks[straightPiece];
      
      return distance;
  };
  // --------------------------------------------
  // returns the straight this piece is into
  // or undefined
  this.pieceStraight = function(pieceIdx) {
    return _.find(_straights, function(s) {
      if (s.startPiece <= s.endPiece) {
        return (s.startPiece <= pieceIdx && 
                s.endPiece >= pieceIdx);
      } else {
        return (s.startPiece <= pieceIdx ||
                s.endPiece >= pieceIdx);
      }
    }) ;
  };
  // --------------------------------------------
  // tells if this is the last straight of the race
  this.isLastStraight = function(pieceIdx) {
    var straight = this.pieceStraight(pieceIdx);
    return (race.lastLap() && 
            straight == this.pieceStraight(0) &&
            pieceIdx >= straight.startPiece
           );
  };
  
  // --------------------------------------------
  // tells if a piece is in the best place to
  // fire turbo
  this.canFireTurbo = function(pieceIdx) {
    // at start of last longest straight
    var bestLen = _straights[0].length
      , nextPieceIdx = (pieceIdx + 1) % this.pieces
      , p = _pieces[pieceIdx]
      , straight = _.find(_.sortBy(_straights, 'startPiece').reverse(), function(s) {
          if (s.length != bestLen)
            return false;
          return (s.startPiece == pieceIdx) ||
                 (p.length && s.startPiece == nextPieceIdx);  // p is a switch contiguous with a straight
        })
      ;

    return (straight !== undefined);
  };

  this.logStats = function() {
    logger.info('Piece count', _pieceCount);
    logger.info('Piece stats');
    _.each(_pieces, function(p) {
      logger.info(p);
    });
  };

//   console.log(this.matchPolicy([26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45],[1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]));
//   throw 10;
};
