/* ********************************************
 * Logger
 *
 * disabled methods will be reduced to identity 
 * function
 */
var Logger = function(mask) {
  var _lastPrefx
    , _line = Array(81).join('-')
    , _crlf = '\r\n'
    ;
  function format(args, prefx) {
    var rt = _.map(args, function(a) {
      return (_.isObject(a) ? JSON.stringify(a) : a);
    });
    if (prefx && prefx !== _lastPrefx) {
      rt.unshift(prefx + ' ' + _line + _crlf);
    }
    _lastPrefx = prefx;

    return rt;
  };

  this.send = function(msg) {
    console.log('--->', JSON.stringify(msg));
  };
  this.recv = function(msg) {
    console.log('<---', JSON.stringify(msg));
  };
  this.lane = function() {
    console.log.apply(null, format(arguments, 'LANE'));
  };
  this.turbo = function() {
    console.log.apply(null, format(arguments, 'TURBO'));
  };
  this.grip = function() {
    console.log.apply(null, format(arguments, 'GRIP'));
  };
  this.brk = function() {
    console.log.apply(null, format(arguments, 'BREAK'));
  };
  this.path = function() {
    console.log.apply(null, format(arguments, 'PATH'));
  };
  this.status = function() {
    console.log.apply(null, format(arguments, 'STAT'));
  };
  this.info = function() {
    console.log.apply(null, format(arguments, 'INFO'));
  };
  this.warning = function() {
    console.warn.apply(null, format(arguments, 'WRN'));
  };
  this.error = function() {
    console.error.apply(null, format(arguments, 'ERR'));
  };
  this.debug = function() {
    console.log.apply(null, format(arguments, 'DBG'));
  };

  // set masked methods to identity
  _.each(_.functions(this), function(fname) {
    if (!mask[fname])
      this[fname] = function() {};
  }, this);

  console.log('PARAMS', _line, _crlf, JSON.stringify(K));
};

module.exports = function(mask) {
  var fullMask = _.defaults(mask || {}, {
      send: false
    , recv: false

    , status: false
    , lane: false
    , grip: false
    , brk: false
    , turbo: false
    , path: false

    , info:    true
    , warning: true
    , error:   true
    , debug:   false
  });
  
  return new Logger(fullMask);
};
