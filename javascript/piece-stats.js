/* ********************************************
 * Metric stat gatherer
 *
 * accumulates values for some metric and performs
 * max + avg calculation
 */
var Metric = function() {
  this.avgs = [];
  this.avg = null;
  this.max = null;

  var _samples = [];
  // reset samples and max
  this.reset = function() {
    _samples = [];
    this.max = null;
  };
  // add a sample, update max
  this.add = function(v) {
    _samples.push(v);
    if (v > (this.max || 0))
      this.max = v;
  };
  // compute average and reset
  this.commit = function() {
    if (_samples.length) {
      if (this.avg !== null)
        this.avgs.push(this.avg);
      this.avg = _.sum(_samples) / _samples.length;
      this.reset();
    }
  };
};

/* ********************************************
 * Piece statistics
 *
 * owns speed and angle metrics
 * keeps track of overall max values
 */
module.exports = function() {
  this.crashes = 0;
  this.successes = 0;
  // metrics in last visit
  this.last = {
    speed: new Metric()
  , angle: new Metric()
  };
  this.max = {
    speed: null
  , angle: null
  };
  // speed value to compare for AI
  this.speed = function() {
    return this.last.speed.avg;
  };
  // angle value to compare for AI
  this.angle = function() {
    return this.last.angle.avg;
  };
  // add a sample
  this.update = function(speed, angle) {
    this.last.speed.add(speed);
    this.last.angle.add(angle);
  };
  // compute metrics avg
  // save overall max
  this.commit = function() {
    this.last.speed.commit();
    this.last.angle.commit();
    if (this.last.speed.avg > (this.max.speed || 0))
      this.max.speed = this.last.speed.avg;
    if (this.last.angle.avg > (this.max.angle || 0))
      this.max.angle = this.last.angle.avg;
    
    this.reset();
  };
  // reset metrics to prepare for next visit
  this.reset = function() {
    this.last.speed.reset();
    this.last.angle.reset();
  };
  // register crash
  this.crash = function() {
    ++ this.crashes;
    this.reset();
  };
  // register success
  this.success = function() {
    ++ this.successes;
    this.commit();
  }

};