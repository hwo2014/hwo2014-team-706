var OpponentStats = require('./opponent-stats.js');
var OpponentBests = require('./opponent-bests.js');

/* ********************************************
 * Opponent
 *
 * Holds opponent status
 */

module.exports = function(logger, carData, race) {
  var _track = race.track
    , _prevPos
    , _prevPiece

    , _curPos
    , _curPiece
    , _curTick
    , _myBests = new OpponentBests(_track)
    ;

  this.color    = carData.id.color;
  this.length   = carData.dimensions.length;
  this.name     = carData.id.name;
  this.position = undefined;
  this.lane     = undefined;
  this.speed    = 0;
  this.angle    = 0;
  this.crashed  = false;
  this.lap      = 0;
  this.distanceFromMyCar = undefined;
  // generate stats
  this.stats = new OpponentStats(_track, carData.id.color);

  /* ********************************************
   * lap increase and car status reset
   */

  var _lapTime = {
    sample: []
  , avg: Number.MAX_VALUE
  , best: Number.MAX_VALUE
  }
  // --------------------------------------------
  // register new lap
  this.newLap = function(ms) {
    ++ this.lap;
    _lapTime.sample.push(ms);
    _lapTime.avg = _.sum(_lapTime.sample) / _lapTime.sample.length;
    if (ms < _lapTime.best)
      _lapTime.best = ms;
  }
  // --------------------------------------------
  // returns reference lap time
  this.lapTime = function() {
    return _lapTime.avg;
  };
  // --------------------------------------------
  // reset initial status
  this.reset = function() {
    this.position = undefined;
    this.lane     = undefined;
    this.speed    = 0;
    this.angle    = 0;
    this.crashed  = false;
    this.lap      = 0;
    this.distanceFromMyCar = undefined;
  };
  
  /* ********************************************
   * crash and spawn
   */

  this.spawn = function() {
    this.crashed  = false;
  };

  this.crash = function() {
    this.crashed  = true;
    this.throttle = 0;
    this.speed    = 0;
    this.angle    = 0;

    this.stats.crash(_curPos.pieceIndex);
  };

  /* ********************************************
   * speed and position
   */

  // compute opponent speed based on previous and current distance
  var _evalSpeed = _.bind(function() {
    var deltaSpace = (_curPos.inPieceDistance - _prevPos.inPieceDistance)
      , deltaTime  = (_curTick - this.position.gameTick)
      , pieceChanged = (_curPos.pieceIndex != _prevPos.pieceIndex)
      ;
    if (pieceChanged) {
      deltaSpace += _track.pieceLength(_prevPos.pieceIndex, 
                                       _prevPos.lane.startLaneIndex, 
                                       _prevPos.lane.endLaneIndex);
      _prevPiece = _track.piece(_prevPos.pieceIndex);
    }

    return (deltaSpace / deltaTime);
  }, this);

  // set position related info, without
  // updating instance variable yet
  var _setPosition = _.bind(function(curPosition) {
    _curPos  = curPosition.piecePosition;
    _curTick = curPosition.gameTick;
    _curPiece  = _track.piece(_curPos.pieceIndex);

    this.lane = _curPos.lane.endLaneIndex;
    this.angle = curPosition.angle;
  }, this);

  // --------------------------------------------
  // set start position
  this.setStartPosition = function(curPosition) {
    _setPosition(curPosition);
    this.position = curPosition;
  }
  // --------------------------------------------
  // on new position, update status 
  this.evalNewPosition = function(curPosition) {
    if (this.crashed)
      return;

    _prevPos = this.position.piecePosition;
    _setPosition(curPosition);
    this.speed    = _evalSpeed();
    this.position = curPosition;

    if (race.opponentsNear(this)) {
      this.stats.reset();
    } else {
      var newSample = this.stats.push(_curTick, this.speed, this.angle, _curPos);
      if (newSample) {
        _track.bests.evaluate(newSample);
        _myBests.evaluate(newSample);
      }
    }
  };

  /* ********************************************
   * overtake decision support
   */

  // --------------------------------------------
  // tells in which track portion we are
  this.switchPieceIndex = function() {
    return _track.switchPieceIndex(_curPos.pieceIndex);
  };

  // --------------------------------------------
  // tells if this opponent is a kill target
  this.killTarget = function(myCar, laneIdx) {
    if (this.crashed || // crashed
        this.lane != laneIdx || // on different lane
        this.lapTime() > myCar.lapTime() + 0.2 || // going slower
        _track.piece((_curPos.pieceIndex+1) % _track.pieces).radius // next is bend
       )
      return false;

    var myCarPos = myCar.position.piecePosition;
    if (_curPos.pieceIndex > myCarPos.pieceIndex)
      return (_curPos.pieceIndex - myCarPos.pieceIndex < 3);
    else
      return (myCarPos.pieceIndex + _track.pieces - _curPos.pieceIndex < 3);
  };  
  // --------------------------------------------
  // tells if this opponent is close to another 
  // car travelling slower in some lane
  this.closeTo = function(myCar, laneIdx) {
    if (this.crashed || // crashed
        this.lane != laneIdx || // on different lane
        this.lapTime() <= myCar.lapTime() + 0.2  // going faster
       )
      return false;

    var myCarPos = myCar.position.piecePosition;
    if (_curPos.pieceIndex > myCarPos.pieceIndex)
      return (_curPos.pieceIndex - myCarPos.pieceIndex < 3);
    else
      return (myCarPos.pieceIndex + _track.pieces - _curPos.pieceIndex < 3);
  };
};