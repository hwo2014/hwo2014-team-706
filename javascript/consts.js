module.exports.DUMP = false;
module.exports.G = 9.81;
module.exports.VT = 10;
module.exports.DECEL = 0.0011;
module.exports.DRAG = 0.1;
module.exports.CAR_MASS = 4.9;
module.exports.ADJUST_FACTOR = 2/3;
module.exports.BEND_LIMITS = {
  "200": {
    speed: 9.275 * (module.exports.VT / 10)
  , raise: 0.02
  , lower: 0.02
  , angle: 55
  }
, "100": {
    speed: 5.5/*6*//*.5*/ * (module.exports.VT / 10)
  , raise: 0.0075
  , lower: 0.0075
  , angle: 57
  }
, "50":  {
    speed: 4.2/*4.6 */* (module.exports.VT / 10)
  , raise: 0.005
  , lower: 0.005
  , angle: 58
  }
, "0": {
    speed: 0
  , raise: 0
  , lower: 0
  , angle: 58
  }
};
