var Turbo = require('./turbo.js');
var Brake = require('./brake.js');
var OpponentStats = require('./opponent-stats.js');
var OpponentBests = require('./opponent-bests.js');

/* ********************************************
 * Car
 *
 * Holds car status and provides drive logic
 */
module.exports = function(logger, yourCarData, race) {
  // local variables used during action evaluation
  // when obtaining new position: current and previous
  // position related information, plus turbo
  var _track = race.track
    , _laneChangeEvalued = false

    , _prevPos
    , _prevPiece

    , _curPos
    , _curPiece
    , _curPieceLength 
    , _curTick

    , _nextPieceIdx
    , _nextPiece

    , _turbo
    , _brake = new Brake(logger)
    , _stats = new OpponentStats(_track, yourCarData.data.color)
    , _myBests = new OpponentBests(_track)
    , _watchOthers = false
    , _crashCount = 0
    , _mass = null
    ;


  // public information
  this.color    = yourCarData.data.color;
  this.length   = null;
  this.name     = yourCarData.data.name;
  this.position = undefined;
  this.lane     = undefined;
  this.throttle = 0;
  this.speed    = 0;
  this.angle    = 0;
  this.crashed  = false;
  this.lap      = 0;
  this.deltaSpace = 0;

  /* ********************************************
   * lap increase and car status reset
   */
  var _lapTime = {
    sample: []
  , avg: Number.MAX_VALUE
  , best: Number.MAX_VALUE
  }
  // --------------------------------------------
  // register new lap
  this.newLap = function(ms) {
    if (++ this.lap > 0)
      _watchOthers = true;
    _lapTime.sample.push(ms);
    _lapTime.avg = _.sum(_lapTime.sample) / _lapTime.sample.length;
    if (ms < _lapTime.best)
      _lapTime.best = ms;
    _brake.reset();
  };
  // --------------------------------------------
  // returns reference lap time
  this.lapTime = function() {
    return _lapTime.avg;
  };
  // --------------------------------------------
  // reset initial status
  this.reset = function() {
    this.position = undefined;
    this.lane     = undefined;
    this.throttle = 0;
    this.speed    = 0;
    this.angle    = 0;
    this.crashed  = false;
    this.lap      = 0;
    _brake.reset();
  };
  
  /* ********************************************
   * turbo
   */
  
  // --------------------------------------------
  // turbo available
  this.addTurbo = function(params) {
    if (!_turbo || _turbo.isDisposable()) {
      logger.turbo('Turbo add for', params.ticks, 'from', params.gameTick);
      _turbo = new Turbo(params);
    }
  };
  // --------------------------------------------
  // tells if turbo can be fired
  this.canUseTurbo = function() {
    if (_turbo && 
        _turbo.isAvailable(_curTick) &&
        Math.abs(this.angle) < 30 &&
        _track.canFireTurbo(_curPos.pieceIndex)) 
    {
      logger.turbo('Turbo start request at', _curTick);
      _turbo.book();
      return true;
    }
    return false;
  };
  // --------------------------------------------
  // registers turbo start
  this.startTurbo = function(tick) {
    if (_turbo) {
      logger.turbo('Turbo start at', tick, 'in', _curPos.pieceIndex, 'speed', this.speed);
      _turbo.start();
    }
  };
  // --------------------------------------------
  // registers turbo stop
  this.stopTurbo = function(tick) {
    if (_turbo) {
      logger.turbo('Turbo stop at', tick, 'in', _curPos.pieceIndex, 'speed', this.speed);
      _turbo.stop();
    }
  };

  this.removeTurbo = function() {
    logger.turbo('Turbo delete at', _curTick);
    if (_turbo) {
      delete(_turbo), _turbo = undefined;
	}
  };
  /* ********************************************
   * crash and spawn
   */

  // --------------------------------------------
  // toggle crashed flag on spawn
  this.spawn = function() {
    logger.info('Respawned');
    this.crashed  = false;
    _laneChangeEvalued = false;
    // refresh switch policy
    _switchPolicy = _track.switchRunningPolicy(this.lane, this.lane, _switchStep);
    logger.lane('Refreshed policy for lane', this.lane, 'at', _switchStep, _switchPolicy);
  };

  // --------------------------------------------
  // on crash, register event in piece limit
  // lower limits of this piece
  // lower limits of previous piece if it's a bend
  this.crash = function() {
    var bestCurSpeed = this.maxCurrentSpeed(this.speed);

    logger.info('Crashed at speed', this.speed, 'throttle', this.throttle, 'best speed', bestCurSpeed);
    logger.info(_curPiece);
    logger.info(_curPos);
//    logger.info(_brake);

    // only if crashed at a speed lower than speed limit
    // if cur piece is bend, lower its limits
    // or if prev piece is bend, lower its limits
    if (_curPiece.radius) 
    {
      if (_curPiece.isSpeedUnderLimit(this.lane, this.speed)) {
        _curPiece.crash(this.lane);
        _track.lowerLimits(_curPiece, this.lane, this.speed);
      }
    }
    else if (_prevPiece && _prevPiece.radius)
    {
      var prevLaneIdx = _prevPos.lane.endLaneIndex;
      if (_prevPiece.isSpeedUnderLimit(prevLaneIdx, this.speed)) {
        _prevPiece.crash(prevLaneIdx);
        _track.lowerLimits(_prevPiece, this.lane);
      }
    }

    this.crashed  = true;
    this.throttle = 0;
    this.speed    = 0;
    _stats.crash(_curPos.pieceIndex);

    if (!race.opponentsNearMe()) {
      logger.info('Lowering bests');
      _track.bests.crash(_curPos.pieceIndex, 
                         _curPos.lane.startLaneIndex,
                         _curPos.lane.endLaneIndex);
      if (_switchPolicy) {
        _track.bests.crash(_prevPiece.index, 
                           _track.switchLane(_prevPiece.index, _switchPolicy),
                           _curPos.lane.startLaneIndex);
      }
    }
    if (++_crashCount > 1)
      _watchOthers = true;
    
    this.removeTurbo();
  };

  /* ********************************************
   * speed
   */

  // computes new speed as ds / dt given current and previous position
  // raise limits of previous (exiting) piece
  var _evalSpeed = _.bind(function() {
    var deltaSpace = (_curPos.inPieceDistance - _prevPos.inPieceDistance)
      , deltaTime  = (_curTick - this.position.gameTick)
      , pieceChanged = (_curPos.pieceIndex != _prevPos.pieceIndex)
      , speed
      ;
    // if piece changed, add previous piece length
    // and reset flag to perform next lane change evaluation
    if (pieceChanged) {
      deltaSpace += _track.pieceLength(_prevPos.pieceIndex,
                                       _prevPos.lane.startLaneIndex, 
                                       _prevPos.lane.endLaneIndex);
      _laneChangeEvalued = false;
      // adjust bend max speed, except if last qualification lap
      _prevPiece = _track.piece(_prevPos.pieceIndex);
      logger.status('Out of piece', _prevPos.pieceIndex,
                    'Speed:', this.speed, 
                    'Throttle:', this.throttle, 
                    'Angle:', this.angle);
    }

    speed = (deltaSpace / deltaTime);
    this.deltaSpace = deltaSpace;

    if (pieceChanged) {
      var prevLaneIdx = _prevPos.lane.endLaneIndex
        ;
      if (_prevPiece.radius) {
        if (_prevPiece.isAngleAboveLimit(prevLaneIdx)) {
          _prevPiece.crash(prevLaneIdx);
          _track.lowerLimits(_prevPiece, prevLaneIdx);
        } else {
          var prevSpeed = _prevPiece.stats[prevLaneIdx].speed();
          if (prevSpeed === null) {
            _prevPiece.stats[prevLaneIdx].commit();
            prevSpeed = _prevPiece.stats[prevLaneIdx].speed();
          }
          var prevSpeedLimit = _prevPiece.speedLimit(prevLaneIdx);
          // if close to limits, mark success
          if ((prevSpeed > prevSpeedLimit) ||
              (prevSpeedLimit - prevSpeed <= 0.1))
            _prevPiece.success(prevLaneIdx);
          else
            _prevPiece.stats[prevLaneIdx].commit();
          // if never crashed, and close to limits, increase them
          if (!_prevPiece.crashed(prevLaneIdx) &&
              closeTo(prevSpeedLimit, prevSpeed, 0.1))
            _track.raiseLimits(_prevPiece, prevLaneIdx, prevSpeed);
        }
      } else {
        _prevPiece.stats[prevLaneIdx].commit();
      }
    }

    return speed;
  }, this);

  /* ********************************************
   * lane change
   */

  // switch policy
  // step in switch policy
  var _switchPolicy
    , _switchStep = 1
    ;

  // --------------------------------------------
  // tells in which track portion we are
  this.switchPieceIndex = function() {
    return _switchStep;
  };

  // may return false, Left or Right
  // depending on the relation between 
  // idealLane and this.lane
  var _getLaneChange = _.bind(function(idealLane) {
    if (idealLane < this.lane) {
      logger.lane('Move left at', _switchStep, 'piece', _curPiece.index);
      return 'Left';
    }
    if (idealLane > this.lane) {
      logger.lane('Move right at', _switchStep, 'piece', _curPiece.index);
      return 'Right';
    }

    return false;
  }, this);

  // set best switch policy 
  var _setupSwitchPolicy = _.bind(function() {
    if (!_curPos.pieceIndex) {
      // init policy at start based on current lane
      _switchPolicy = _track.switchStartPolicy(this.lane);
      _switchStep = 1;
      logger.lane('Start policy for lane', this.lane, _switchPolicy);
    } else if (!_switchPolicy) {
      // init policy at non-start based on current lane
      _switchStep  = _track.switchPieceIndex(_curPos.pieceIndex);
      _switchPolicy = _track.switchRunningPolicy(this.lane, this.lane, _switchStep);
      logger.lane('Policy for lane', this.lane, 'at', _switchStep, _switchPolicy);
    } else if (_switchPolicy.lanes[_switchStep-1] != this.lane) {
      // if not in expected lane, switch policy accordingly
      logger.lane('Unexpected policy for lane', this.lane, 'at', _switchStep, _switchPolicy);
      _switchPolicy = _track.switchRunningPolicy(this.lane, this.lane, _switchStep);
      logger.lane('Fixed policy for lane', this.lane, 'at', _switchStep, _switchPolicy);
    }
    // look for best strategy from opponents
    var bestStrategy = (_watchOthers 
          ? _track.bests.futurePath(_curPos.pieceIndex, this.lane) 
          : false
        )
      ;
    logger.lane('Best strategy from opponents', bestStrategy);
    if (bestStrategy) {
      if (bestStrategy.lane != this.lane) {
        // update policy and apply
        _switchPolicy = _track.matchPolicy(bestStrategy.pieces, bestStrategy.lanes)
          || _track.switchRunningPolicy(this.lane, bestStrategy.lane, _switchStep);
        logger.lane('Policy for best path', this.lane, '->', bestStrategy.lane, 'at', _switchStep, _switchPolicy);
      }
    }
  }, this);
  
  // set collision switch policy
  var _setupCollisionSwitchPolicy = _.bind(function() {
    var collider = race.collisionTarget(this.lane)
      , bestCollider = collider
      , bestLane = this.lane;
    // if we have a collider, look for adjacent lane
    // that will be chosen if there is no collision
    // expected there, or a collition farther than
    // current
    if (bestCollider && this.lane > 0) {
      collider = race.collisionTarget(this.lane-1);
      if (!collider || collider.distanceFromMyCar > bestCollider.distanceFromMyCar) {
        bestCollider = collider;
        bestLane = this.lane-1;
      }
    }
    if (bestCollider && this.lane+1 < _track.lanes) {
      collider = race.collisionTarget(this.lane+1);
      if (!collider || collider.distanceFromMyCar > bestCollider.distanceFromMyCar) {
        bestCollider = collider;
        bestLane = this.lane+1;
      }
    }
    if (bestLane !== this.lane) {
      // check for a known path in that direction
      if (_track.bests.futurePath(_switchPolicy.pieces[_switchStep]+1, bestLane) !== false) {
        _switchPolicy = _track.switchRunningPolicy(this.lane, bestLane, _switchStep);
        logger.lane('Policy for collision', this.lane, '->', bestLane, 'at', _switchStep, _switchPolicy);
        return true;
      }
    }
    return false;
  }, this);
  
  // evaluates need to change lane based on global path length
  var _evalLaneChangeGlobal = _.bind(function() {
    // if next piece is a switch, change lane according to policy
    if (_nextPieceIdx == _switchPolicy.pieces[_switchStep]) {
      var nextLaneIdx = _switchPolicy.lanes[_switchStep]
        , curStep     = _switchStep
        ;
      _switchStep = ((_switchStep + 1) % _switchPolicy.lanes.length) || 1;
      return _getLaneChange(nextLaneIdx);
    } else {
      return false;
    }
  }, this);

  /* ********************************************
   * throttle up / down
   */

  // get max speed to keep watching around
  this.maxCurrentSpeed = function(newSpeed) {
    if (_watchOthers) {
      var futureSpeed = _track.bests.speedInPosition(
        _curPos.pieceIndex, 
        _curPos.lane.startLaneIndex, 
        _curPos.lane.endLaneIndex,
        _curPos.inPieceDistance, newSpeed, 
        _.bind(_brake.distance, _brake), 
        _switchPolicy);
      return futureSpeed;
    }
  };
  this.maxCurrentAngle = function(newSpeed) {
    if (_watchOthers) {
      var futureAngle = _track.bests.angleInPosition(
        _curPos.pieceIndex, 
        _curPos.lane.startLaneIndex, 
        _curPos.lane.endLaneIndex,
        _curPos.inPieceDistance, newSpeed);
      return futureAngle;
    }
  };
  // get best speed to reach
  this.bestEnterSpeed = function(pieceIdx, fromLane, toLane) {
    if (_watchOthers) {
      var rt = _track.bests.futureSpeed(pieceIdx, fromLane, toLane);
//       console.log('best next speed piece', pieceIdx, 'lane', fromLane, '>', toLane, rt);
      return rt;
    }
  }
  // --------------------------------------------
  // distance travelled in current lap according
  // to current switch policy - may not stick to truth
  // since it considers a policy that have changed
  // during time
  this.travelledDistance = function() {
    return _track.travelledDistance(_curPos.pieceIndex, 
                                    _curPos.inPieceDistance, 
                                    _switchPolicy);
  };

  // evaluates throttle delta
  // if time to brake for next piece, set delta to -current, so it goes to 0
  //  base push if in a bend and going too fast or too drift
  var _evalThrottle = _.bind(function(newSpeed, newLane) {
    var speedLimit = this.maxCurrentSpeed(newSpeed)
      , angleLimit = Math.abs(this.maxCurrentAngle(newSpeed) || _curPiece.angleLimit(this.lane))
      , reduce = false
      , angle  = this.angle
      ;
    if (!_track.isLastStraight(_curPos.pieceIndex)) {
      if (speedLimit === undefined || speedLimit === null || (_turbo && _turbo.inUse() && _curPiece.length)) {
        speedLimit = _curPiece.speedLimit(this.lane);
        // init next piece and distance to it
        var nextPiece = _nextPiece
          , distanceToNextPiece = (_curPieceLength - _curPos.inPieceDistance)
          ;

        // get next bend after a staight and update nextPiece and distance to it
        var nextBendAfterStraight = _track.bendAfterStraight(_curPos.pieceIndex);
        if (nextBendAfterStraight) {
          nextPiece = nextBendAfterStraight.piece;
          distanceToNextPiece = _track.distanceToBendAfterStraight(nextBendAfterStraight, _curPos.inPieceDistance);
        }
        // here, i know in which piece to brake

        // get opponents speed in next bend 
        // compute speed limit and brake distance
        var bestEnterSpeed = this.bestEnterSpeed(
              nextPiece.index, 
              _curPos.lane.endLaneIndex, 
              _track.switchLane(nextPiece.index, _switchPolicy)
            )
          , nextLimit = bestEnterSpeed || nextPiece.speedLimit(this.lane)
          , brakeDistance = _brake.distance(newSpeed, nextLimit)
          ;
        // brake in any piece to enter next bend at defined speed
        brakeDistance *= (_turbo && _turbo.inUse() ? 1.05 : 1);
        if (distanceToNextPiece <= brakeDistance || distanceToNextPiece + newSpeed <= brakeDistance) {
          logger.brk('brake start in', _curPos.pieceIndex, 'at', newSpeed, 'to reach', nextLimit, _curTick);
          logger.brk('next piece at', distanceToNextPiece, 'brake distance', brakeDistance);
          // save brake start info to eval DECEL
          if (nextBendAfterStraight && !_brake.isSampling()) {
            _brake.startSample({
              v: newSpeed
            , s: this.travelledDistance()
            }, {
              v: nextLimit
            , piece: nextBendAfterStraight.pieceIndex
            });
          }
          return 0;
        }
      }

      if (_curPiece.radius && sign(angle) != sign(_curPiece.angle))
        angle = 0;
      angle = Math.abs(angle);

      if ((_curPiece.radius || (_curPiece.switch && newLane !== false)) && 
          Math.ceil(angle) > Math.floor(angleLimit) && 
          speedLimit < newSpeed * 1.15) 
      {
        if (angle - angleLimit > 5 && angle < 30)
          speedLimit = 0;
        else
          reduce = (angle < 15);
      }
    }

    var finalize = function(throttle) {
      if (_curPiece.radius && _turbo && _turbo.inUse())
        return throttle / _turbo.factor;
      return throttle * (reduce ? 0.9 : 1);
    }
    
    // push
    var newThrottle = _brake.drag * (speedLimit - newSpeed * Math.exp(-_brake.drag/_mass)) / (1 - Math.exp(-_brake.drag/_mass));
    return finalize(newThrottle);
  }, this);

  /* ********************************************
   * reaction entry points
   */

  // set position related info, without
  // updating instance variable yet
  var _setPosition = _.bind(function(curPosition) {
    _curPos  = curPosition.piecePosition;
    _curTick = curPosition.gameTick;

    _nextPieceIdx = (_curPos.pieceIndex + 1) % _track.pieces;
    _nextPiece = _track.piece(_nextPieceIdx);

    _curPiece  = _track.piece(_curPos.pieceIndex);
    _curPieceLength = _track.pieceLength(_curPos.pieceIndex, 
                                         _curPos.lane.startLaneIndex, 
                                         _curPos.lane.endLaneIndex);

    this.lane = _curPos.lane.endLaneIndex;
    this.angle = curPosition.angle;
  }, this);

  // computes new lane and throttle
  var _newLaneAndThrottle = _.bind(function(newSpeed) {
    var newLane = false
      , collision = false
      ;
    if (!_laneChangeEvalued) {
      if (!_setupCollisionSwitchPolicy()) {
        _setupSwitchPolicy();
      }
      newLane = _evalLaneChangeGlobal();
      _laneChangeEvalued = true;
    }

    if (_brake.drag === undefined) {
      if (_brake.canEvalDrag()) {
        _mass = _brake.evalDrag(1);
        logger.grip('Drag is', _brake.drag, 'mass', _mass);
      } else {
        _brake.addDragSample(newSpeed);
        this.throttle = 1;
        return { lane: false, throttle: 1 };
      }
    }

    // compute next throttle
    var newThrottle = _evalThrottle(newSpeed, newLane);
    // and cap in [0,1]
    newThrottle = Math.max(0, Math.min(1, newThrottle));

    return {
      lane: newLane
    , throttle: newThrottle
    };
  });
  // --------------------------------------------
  // set first position updating local information
  this.setStartPosition = function(curPosition) {
    _setPosition(curPosition);
    this.position = curPosition;
  };
  // --------------------------------------------
  // first throttle and lane switch at gameStart
  this.start = function() {
    var rt = _newLaneAndThrottle(0);

    // update status and stats
    this.throttle = rt.throttle;

    return rt;
  };
  // --------------------------------------------
  // on new position, update status and evaluate
  // new throttle, lane switch and deceleration
  this.evalNewPosition = function(curPosition) {
    // update position
    _prevPos = this.position.piecePosition;
    _setPosition(curPosition);

    // remove used turbo
    if (_turbo && _turbo.isExhausted()) {
      this.removeTurbo();
    }

    // compute speed
    var newSpeed = _evalSpeed();

    // eval deceleration
    if (_brake.target.piece === _curPos.pieceIndex) {
      if (newSpeed > _brake.target.v) {
        _brake.update(newSpeed, this.travelledDistance());
      } else {
        _brake.reset();
      }
    }
    // eval lane change and new throttle
    var rt = _newLaneAndThrottle(newSpeed);

    // update status and stats
    this.position = curPosition;
    this.speed    = newSpeed;
    this.throttle = rt.throttle;

    // we're pushing, so stop brake sampling (early brake !)
    if (this.throttle && _brake.isSampling()) {
      _brake.update(newSpeed, this.travelledDistance());
    }    
    // save stats for auto drive
    _curPiece.stats[this.lane].update(this.speed, this.angle);
    // evaluate my behaviour as for opponents
    if (race.opponentsNear(this)) {
      _stats.reset();
    } else {
      var newSample = _stats.push(_curTick, this.speed, this.angle, _curPos);
      if (newSample) {
        _track.bests.evaluate(newSample, true);
        _myBests.evaluate(newSample);
      }
    }
    return rt;
  };

};
