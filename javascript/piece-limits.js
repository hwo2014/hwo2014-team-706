var PieceStats = require('./piece-stats.js');

/* ********************************************
 * Piece limits
 *
 * owns stats and speed / angle limits
 * guesses the unknown
 */
module.exports = function(piece, lanes) {
  // angle / speed limits per lane
  this.limit = {
  };
  // angle / speed stats per lane
  this.stats = {
  };
  // register crash in lane
  this.crash = function(laneIdx) {
    this.stats[laneIdx].crash();
  };
  // is crashed in lane ?
  this.crashed = function(laneIdx) {
    return (this.stats[laneIdx].crashes > 0);
  };
  // register success in lane
  this.success = function(laneIdx) {
    this.stats[laneIdx].commit();
  };
  // is there some success or crash in lane ?
  this.hasExperience = function(laneIdx) {
    return this.stats[laneIdx].crashes > 0 ||
           this.stats[laneIdx].successes > 0;
  };
  // returns speed limit in lane
  this.speedLimit = function(laneIdx) {
    return this.limit[laneIdx].speed;
  };
  // update speed limit in lane with new value
  this.setSpeedLimit = function(laneIdx, speed) {
    this.limit[laneIdx].speed = speed;
  };
  // modifies speed limit in lane by a factor
  this.changeSpeedLimit = function(laneIdx, factor) {
    this.limit[laneIdx].speed *= factor;
  };
  // is speed under the limit ?
  // if no speed provided, the one from stats is used
  this.isSpeedUnderLimit = function(laneIdx, speed) {
    if (speed === undefined)
      speed = this.stats[laneIdx].speed();
    return (!speed || speed < this.limit[laneIdx].speed);
  };
  // returns angle limit in lane
  this.angleLimit = function(laneIdx) {
    return this.limit[laneIdx].angle;
  }
  // is angle above the limit ?
  // if no angle provided, the one from stats is used
  this.isAngleAboveLimit = function(laneIdx, angle) {
    if (angle === undefined)
      angle = this.stats[laneIdx].angle();
    return (angle && angle >= this.limit[laneIdx].angle);
  };
  
  // init limits
  {
    var s, a, r, l;
    if (piece.radius) {
      var model = K.BEND_LIMITS[piece.radius];
      // guess data for unknown radius
      if (model === undefined) {
        var known = _.sortBy(_.map(_.keys(K.BEND_LIMITS), function(x) { return x*1 }), _.identity)
          , m = 0
          , last = K.BEND_LIMITS[_.last(known)]
          ;
        while(m < known.length && known[m] < piece.radius)
          ++m;
        if (m >= known.length) {
          model = {
            speed: 10,
            raise: last.raise,
            lower: last.lower,
            angle: 58
          };
        } else {
          var r0 = known[m-1]
            , r1 = known[m]
            , l0 = K.BEND_LIMITS[r0]
            , l1 = K.BEND_LIMITS[r1]
            ;
          model = {};
          _.each(['speed', 'raise', 'lower', 'angle'], function(prop) {
            model[prop] = interpolate(piece.radius,r0,r1,l0[prop],l1[prop]);
          });
        }
        // set limits
        K.BEND_LIMITS[piece.radius] = model;
      }
      // set vars from model
      s = model.speed;
      a = model.angle;
      r = model.raise;
      l = model.lower;
    } else {
      s = Number.MAX_VALUE;
      a = 55;
    }
    
    // set limits and stats for each lane
    _.each(lanes, function(lane) {
      this.limit[lane.index] = {
        speed: s
      , angle: a * sign(piece.angle)
      , raise: r
      , lower: l
      };
      this.stats[lane.index] = new PieceStats();
    }, this);
  }
};