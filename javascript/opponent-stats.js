/* ********************************************
 * Piece statistics
 */
var OpponentStats = function(track, color) {
  this.color = color;

  var _storedPieces = 0;
  // add new statistics and returns oldest full stats
  // if it's time to do that
  this.push = function(tick, speed, angle, pos) {
    if (speed > 10) {
      this.reset();
      return;
    }

    var lastPiece = _.last(this.pieces)
      , lastDist  = _.last(this.distances)
      ;
    if (lastPiece != pos.pieceIndex) {
      // use these to set first stat of new piece
      var a = angle, s = speed;
      ++ _storedPieces;
      // fill gap on piece change
      // for previous piece, if any
      // and for current piece
      if (this.pieces.length) {
        var lastIn = _.last(this.lanesIn)
          , lastOut = _.last(this.lanesOut)
          , lastPieceLength = track.pieceLength(lastPiece, lastIn, lastOut)
          ;
        a = interpolate(lastPieceLength,
                        lastDist, lastPieceLength + pos.inPieceDistance,
                        _.last(this.angles), angle);
        s = interpolate(lastPieceLength,
                        lastDist, lastPieceLength + pos.inPieceDistance,
                        _.last(this.speeds), speed);
        // closing stats of last piece
        this.ticks.push(_.last(this.ticks));
        this.pieces.push(lastPiece);
        this.chunks.push(lastPieceLength - lastDist);
        this.distances.push(lastPieceLength);
        this.lanesIn.push(lastIn);
        this.lanesOut.push(lastOut);
        this.angles.push(a);
        this.speeds.push(s);
      }
      // opening stats of new piece
      this.ticks.push(tick);
      this.pieces.push(pos.pieceIndex);
      this.chunks.push(0);
      this.distances.push(0);
      this.lanesIn.push(pos.lane.startLaneIndex);
      this.lanesOut.push(pos.lane.endLaneIndex);
      this.angles.push(a);
      this.speeds.push(s);
    }

    // push current stats
    if (pos.inPieceDistance > 0) {
      // there is always an opening stat for this piece
      this.ticks.push(tick);
      this.pieces.push(pos.pieceIndex);
      this.chunks.push(pos.inPieceDistance - _.last(this.distances));
      this.distances.push(pos.inPieceDistance);
      this.lanesIn.push(pos.lane.startLaneIndex);
      this.lanesOut.push(pos.lane.endLaneIndex);
      this.angles.push(angle);
      this.speeds.push(speed);
    }
    
    if (this.speeds.length > 1) {
      var v0 = this.speeds[this.speeds.length - 2]
        , v1 = this.speeds[this.speeds.length - 1]
        , k = Math.exp(-K.DRAG/K.CAR_MASS)
        , v1min = v0*k - 0.02
        , v1max = 1/K.DRAG * (1-k) + v0*k + 0.02
        ;
      if (!between(v1, v1min, v1max)) {
//         console.log(this.color, 'butta via tutto', v0, v1, v1min, v1max);
        this.reset();
      }
    }
    // if time to pop out, return the oldest stats
    return this.evaluate();
  };
  // reset everything
  this.reset = function() {
    this.pieces = [];
    this.ticks = [];
    this.distances = [];
    this.chunks = [];
    this.angles = [];
    this.lanesIn = [];
    this.lanesOut = [];
    this.speeds = [];
    this.throttles = [];

    this.avgSpeed = null;
    this.inSpeed  = null;
    this.outSpeed = null;

    this.maxAngle = null;
    this.avgAngle = null;
    this.inAngle  = null;
    this.outAngle = null;

    this.length = null;
    _storedPieces = 0;
  };
  // set speeds of crashed piece to 0 to avoid push
  this.crash = function(pieceIndex) {
    for(i = this.pieces.length-1; 
        i >= 0 && this.pieces[i] == pieceIndex; 
        --i)
    {
      this.speeds[i] = 0;
    }
  };
  // computes average, in and out speed for this instance
  // usually result of an extraction
  this.avg = function() {
    var sum = 0
      , length = track.pieceLength(this.pieces[0], this.lanesIn[0], this.lanesOut[0])
      ;
    for(i = 0; i < this.speeds.length; ++i) {
      sum += this.speeds[i] * this.chunks[i];
    }
    this.avgSpeed = sum / length;
    this.inSpeed  = _.first(this.speeds);
    this.outSpeed = _.last(this.speeds);

    this.maxAngle = _.max(this.angles);
    this.avgAngle = _.sum(this.angles) / this.angles.length;
    this.inAngle  = _.first(this.angles);
    this.outAngle = _.last(this.angles);

    this.length = length;

    return this;
  };
  // target speed to reach to obtain speed to keep average
  this.instantTarget = function(target) {
    if (this.speeds.length) {
      var sum = 0
        , travelled = 0
        ;
      for(i = 0; i < this.speeds.length; ++i) {
        sum += this.speeds[i] * this.chunks[i];
        travelled += this.chunks[i];
      }

      return (target * this.length - sum) / (this.length - travelled);
    }
    return target;
  }
  // returns oldest sample if enough pieces are sampled
  // in form of a class instance
  this.evaluate = function() {
    if (_storedPieces > 10) {
      var firstPieceIndex = this.pieces[0]
        , nSamples = 1
        , start = 0;
        ;
      var extract = function(remove) {
        var rt = new OpponentStats(track, this.color)
          , props = ['pieces', 'ticks', 'distances', 'angles', 'lanesIn', 'lanesOut', 'speeds', 'chunks', 'throttles']
          ;
        while(this.pieces[nSamples] == firstPieceIndex) {
          ++nSamples;
        }
        _.each(props, function(prop) {
          if (remove)
            for(i = start; i < nSamples; ++i) {
              rt[prop].push(this[prop].shift());
            }
          else
            for(i = start; i < nSamples; ++i) {
              rt[prop].push(this[prop][i]);
            }
        }, this);

        if (remove) {
          -- _storedPieces;
          nSamples = 1;
          start    = 0;
        } else {
          start = nSamples;
          ++ nSamples;
        }
        firstPieceIndex = this.pieces[start];

        return rt.avg();
      };
      // return first samples
      var rt = [];
      for(p = 0; p < 5; ++p)
        rt.push(extract.call(this, p==0));
      
      return rt;
    }
  };
  
  this.reset();
};

module.exports = OpponentStats;