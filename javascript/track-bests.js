var fs = (false && K.DUMP ? require('fs') : null);

/* ********************************************
 * TrackBests
 *
 * keep track of every piece record for every
 * path run by some car during the race
 * gives insights about the future:
 *  where should one go
 *  how fast should one go
 *  what angle should one keep
 */
var TrackBests = function(track) {

  var FNAME = track.name + '.bests.json';
  var DEPTH = 20;

  var _pieceBests = [];

  // dump stats to file
  this.dump = function() {
    fs && fs.writeFile(FNAME, JSON.stringify(_pieceBests));
  };
  // load stats from file or init
  this.load = function() {
    if (fs && fs.existsSync(FNAME)) {
      _pieceBests = JSON.parse(fs.readFileSync(FNAME));
    } else {
      for(p = 0; p < track.pieces; ++p) {
        _pieceBests.push({});
      };
    }
  };

  // locks bests for the crashed piece and lowers its limits
  this.crash = function(pieceIndex, fromLane, toLane) {
    var pathIndex  = '' + fromLane + toLane
      , down       = 0.92
      , best       = _pieceBests[pieceIndex][pathIndex]
      ;
    console.log('lower', pieceIndex, pathIndex);
    if (best !== undefined) {
      best.locked = true;
      for(i = 0; i < best.speeds.length; ++i) {
        best.speeds[i] *= down;
      }
      best.inSpeed *= down;
      best.outSpeed *= down;
    }
  };
  
  // computes average on new stats and substitutes it in best
  // if avg speed is better
  this.evaluate = function(newStats, log) {
    // fill holes and collect metrics
    var currentRate = 0
      , newRate     = 0
      ;
    _.each(newStats, function(stat) {
      var pieceIndex = stat.pieces[0]
        , pathIndex  = '' + stat.lanesIn[0] + stat.lanesOut[0]
        , best       = _pieceBests[pieceIndex][pathIndex]
        ;
      if (best === undefined) {
        best = _pieceBests[pieceIndex][pathIndex] = stat;
      }
      currentRate += (best.avgSpeed ? (best.length / best.avgSpeed) : 0);
      newRate     += (stat.avgSpeed ? (stat.length / stat.avgSpeed) : 0);
    });

    if (newRate < currentRate) {
      _.each(newStats, function(stat) {
        var pieceIndex = stat.pieces[0]
          , pathIndex  = '' + stat.lanesIn[0] + stat.lanesOut[0]
          , best       = _pieceBests[pieceIndex][pathIndex]
          ;
        if (!best.locked) {
          _pieceBests[pieceIndex][pathIndex] = stat;
        }
      });
    }
  };

  // returns the target speed for a given position
  this.speedInPosition = function(pieceIndex, fromLane, toLane, 
                                  currentPosition, speed, brakeDistance,
                                  switchPolicy
                                 ) 
  {
    // search stats for current piece
    var futurePosition = currentPosition + speed
      , candidateSpeed = undefined
      , pathIndex = '' + fromLane + toLane
      , best      = _pieceBests[pieceIndex][pathIndex]
      ;
    if (best !== undefined) {
      for(i = 0; i < best.distances.length; ++i) {
        if (best.distances[i] > futurePosition) {
          candidateSpeed = interpolate(futurePosition, currentPosition, best.distances[i], speed, best.speeds[i]);
          break;
        }
      }
    }
    // look forward, and update if need to brake to enter correctly one of next pieces
    var spaceLeft = track.pieceLength(pieceIndex, fromLane, toLane) - futurePosition;
    for(i = 0; i < 3; ++i) {
      var nextPieceIndex = (pieceIndex + i + 1) % track.pieces;
      fromLane = toLane;
      toLane = track.switchLane(nextPieceIndex, switchPolicy);

      var nextPathIndex  = '' + fromLane + toLane
        , nextBest       = _pieceBests[nextPieceIndex][nextPathIndex]
        ;
      if (nextBest !== undefined) {
        if (candidateSpeed === undefined || 
            spaceLeft < brakeDistance(speed, nextBest.inSpeed)) 
        {
          candidateSpeed = nextBest.inSpeed;
        }
      }
      spaceLeft += track.pieceLength(nextPieceIndex, fromLane, toLane);
    }

    return candidateSpeed;
  };

  // returns the target angle for a given position
  this.angleInPosition = function(pieceIndex, fromLane, toLane, 
                                  currentPosition, speed) {
    var pathIndex = '' + fromLane + toLane
      , best = _pieceBests[pieceIndex][pathIndex]
      ;
    if (best === undefined) {
      return undefined;
    }
    for(i = 0; i < best.distances.length; ++i) {
      if (best.distances[i] > currentPosition) {
        return best.angles[i];
      }
    }
    return best.outAngle;
  };

  var _bestStats = _.bind(function(pieceIndex, fromLane, toLane) {
    var pathIndex = '' + fromLane + toLane
      , best = _pieceBests[pieceIndex][pathIndex]
      ;
    return best;
  }, this);
  // returns speed to reach on next piece
  this.futureSpeed = function(pieceIndex, fromLane, toLane) {
    var best = _bestStats(pieceIndex, fromLane, toLane);

    return (best ? best.inSpeed : undefined);
  };
  // returns average speed to keep in current piece
  this.currentSpeed = function(pieceIndex, fromLane, toLane) {
    var best = _bestStats(pieceIndex, fromLane, toLane);

    return (best ? best.avgSpeed : undefined);
  };
  // returns average angle to keep in current piece
  this.currentAngle = function(pieceIndex, fromLane, toLane) {
    var best = _bestStats(pieceIndex, fromLane, toLane);

    return (best ? best.avgAngle : undefined);
  };
  // returns switch policy
  this.futurePath = function(pieceIndex, fromLane) {
    var pathIndex = '' + fromLane + fromLane
      , choices = []
      ;
    var nextPieceIndex = (pieceIndex + 1) % _pieceBests.length
      , nextPiece = track.piece(nextPieceIndex)
      ;
    choices.push(_evalPath(nextPieceIndex, fromLane, fromLane, DEPTH, Math.floor(DEPTH/2)));
    if (nextPiece.switch) {
      if (fromLane > 0) {
        choices.push(_evalPath(nextPieceIndex, fromLane, fromLane-1, DEPTH, Math.floor(DEPTH/2)));
      }
      if (fromLane+1 < track.lanes) {
        choices.push(_evalPath(nextPieceIndex, fromLane, fromLane+1, DEPTH, Math.floor(DEPTH/2)));
      }
    }

    var best = _bestPath(choices)
      , exists = (best.rate > 0);

    if (best.rate > 0)
      return best;

    return false;
  };

  // returns a value indicating the goodness of a switch path
  // computed as sum(average speed * piece length). 
  // Recursively, the path with max value is chosen
  var _evalPath = function(pieceIndex, fromLane, toLane, depth, safeDepth) {

    var rt = { 
          rate: 0
        , safe: true
        , lane: toLane
        , pieces: [pieceIndex]
        , lanes: [toLane] 
        }
        ;

    if (depth <= 0)
      return rt;

    var best = _pieceBests[pieceIndex]
      , pathIndex = '' + fromLane + toLane
      ;
    if (best[pathIndex] === undefined)
      return rt;

    rt.rate = (best[pathIndex].avgSpeed * track.pieceLength(pieceIndex, fromLane, toLane));
    if (safeDepth > 0) {
      var angle = best[pathIndex].maxAngle+10;
      rt.safe = (!track.piece(pieceIndex).isAngleAboveLimit(fromLane, angle) &&
                 !track.piece(pieceIndex).isAngleAboveLimit(toLane, angle));
    }
    if (depth == 1) {
      return rt;
    }

    var nextPieceIndex = (pieceIndex + 1) % _pieceBests.length
      , nextPiece = track.piece(nextPieceIndex)
      , choices = []
      ;
    choices.push(_evalPath(nextPieceIndex, toLane, toLane, depth-1, safeDepth-1));
    if (nextPiece.switch) {
      if (toLane > 0) {
        choices.push(_evalPath(nextPieceIndex, toLane, toLane-1, depth-1, safeDepth-1));
      }
      if (toLane+1 < track.lanes) {
        choices.push(_evalPath(nextPieceIndex, toLane, toLane+1, depth-1, safeDepth-1));
      }
    }

    var best = _bestPath(choices);
    best.rate += rt.rate;
    best.safe &= rt.safe;
    best.pieces.unshift(pieceIndex);
    best.lanes.unshift(toLane);
    return best;
  };

  // returns the path with max rate given a set of choices
  var _bestPath = function(choices) {
    var max  = 0
      , rt = undefined
      ;
    _.each(choices, function(c) {
      if (c.rate >= max) {
        max = c.rate;
        rt = c;
      }
    });
    return rt;
  };
};

module.exports = TrackBests;