var statLog = (K.DUMP ? require('fs').createWriteStream("throttle.log") : null);
var Race = require('./race.js');

var toLog = function(x) {
  return (x + '').replace(/\./, ',');
}

/* ********************************************
 * Protocol implementation
 * 
 * Has methods to send data over client socket
 * and use received json messages
 */
module.exports = function(logger, client) {

  var _race = new Race(logger)
    ;

  /* ********************************************
   * Outbound
   */

  // low level send
  var _send = _.bind(function(json, logMessage) {
    logger.send(logMessage || json);
    client.write(JSON.stringify(json));
    client.write('\n');
  }, this);

  // --------------------------------------------
  // create race
  // join / createRace depending on 
  // trackName presence
  this.startServer = function(botName, botKey, 
                              trackName, slots) {
    if (trackName) {
      _send({
          msgType: 'createRace'
        , data: {
            botId: {
              name: botName
            , key: botKey
            }
          , trackName: trackName
          , carCount: slots || 1
          }
      });
    } else {
      _send({
          msgType: 'join'
        , data: {
            name: botName
          , key: botKey
          }
      });
    }
  };

  this.joinServer = function(botName, botKey, 
                             trackName, trackPass, 
                             slots) {
      _send({
          msgType: 'joinRace'
        , data: {
            botId: {
              name: botName
            , key: botKey
            }
          , trackName: trackName
          , password: trackPass
          , carCount: slots || 2
          }
      });
  };
  // --------------------------------------------
  // send ping
  this.ping = function() {
    _send({
        msgType: 'ping'
    });
  };

  // --------------------------------------------
  // stay quiet
  this.silence = function() {};

  /* ********************************************
   * Inbound
   */

  // --------------------------------------------
  // receive join confirm
  this.join = function(message) {
    logger.debug(message.data.name, 'joined the server');
    return this.ping;
  };
  
  this.joinRace = function(message) {
    logger.debug(message.data.name, 'joined the server');
    return this.ping;
  };
  // --------------------------------------------
  // receive create race confirm
  this.createRace = function(message) {
    logger.debug(message.data.botId.name, 'joined the server at', message.data.trackName);
    if (message.data.carCount > 1) {
      logger.info('Waiting for opponents');
    }
    return this.silence;
  }
  // --------------------------------------------
  // receive your car info
  // create new Car in top-level variable
  this.yourCar = function(message) {
    _race.setCarData(message);
    return this.silence;
  };
  // --------------------------------------------
  // receive game init info
  // create new Track in top-level variable
  this.gameInit = function(message) {
    _race.setTrackData(message);
//     _race.track.logStats();
    return this.silence;
  };
  // --------------------------------------------
  // receive game start - we're racing
  this.gameStart = function(message) {
    logger.info((_race.qualifications ? 'Qualifications' : 'Race'), 'started');
    _race.active = true;
    var action = _race.car.start();
    return _drive(message.gameTick, action.throttle, action.lane);
  };

  // --------------------------------------------
  // reveive game end - not racing anymore
  this.gameEnd = function(message) {
    logger.info((_race.qualifications ? 'Qualifications' : 'Race'), 'ended');

    _race.active = false;
    _race.qualifications = false;

    _race.track.resetLowerLimitFactors();
//     _race.track.logStats();

    return this.silence;
  };
  // --------------------------------------------
  // tournament end - very end of games
  this.tournamentEnd = function(message) {
    logger.info('Tournament ended');
//     _race.logStats();
    statLog && statLog.close();
    _race.track.bests.dump();
    return this.ping;
  };
  // --------------------------------------------
  // car crashed
  this.crash = function(message) {
    _race.crash(message.data.color);
    return this.silence;
  };
  // --------------------------------------------
  // car respawned
  this.spawn = function(message) {
    _race.spawn(message.data.color);
    return this.silence;
  };

  // send throttle or switchLane
  function _drive(tick, throttle, lane) {
    if (lane) {
      return function() {
        _send({
          msgType: 'switchLane'
        , data: lane
        , gameTick: tick
        });
      };
    } else {
      return function() {
        _send({
          msgType: 'throttle'
        , data: throttle
        , gameTick: tick
        });
      };
    }
  };
  // send turbo request
  function _boost(tick) {
    return function() {
      _send({
        msgType: 'turbo'
      , data: "Pogue Mahone's !"
      , gameTick: tick
      });
    };
  };
  // --------------------------------------------
  // apply changes to car behaviour given
  // new car positions
  this.carPositions = function(message) {
    var tick = message.gameTick || 0;
    _race.time = tick / 60 * 1000;
    if (!_race.active) {
      // if not yet racing, set starting position
      // and return
      _.each(message.data, function(position) {
        position.gameTick = tick;
        _race.opponents[position.id.color].setStartPosition(position);
      });
      return this.ping;
    } else {
      var myPosition = null;
      _.each(message.data, function(position) {
        position.gameTick = tick;
        if (_race.car.color == position.id.color) {
          myPosition = position;
        } else {
          _race.opponents[position.id.color].evalNewPosition(position);
        }
      });

      if (_race.car.crashed) {
        return this.ping;
      } else if (_race.car.canUseTurbo()) {
        // request turbo if feasible
        return _boost(myPosition.gameTick);
      } else {
        // take action: will affect car stats
        // and return new lane if a switch is
        // required
        var action = _race.car.evalNewPosition(myPosition);
        statLog && statLog.write(
          myPosition.piecePosition.pieceIndex + "\t" + 
          toLog(action.throttle) + "\t" +
          toLog(_race.car.deltaSpace) + "\t" +
          toLog(_race.car.angle) + "\t" +
          toLog(_race.car.maxCurrentSpeed(_race.car.speed)) + "\t" +
          toLog(_race.car.maxCurrentAngle(_race.car.speed)) + "\n"
        );

        return _drive(tick, action.throttle, action.lane);
      }
    }
  };
  // --------------------------------------------
  // give turbo to car
  this.turboAvailable = function(message) {
//     return this.silence;
    if (!_race.car.crashed) {
      _race.car.addTurbo({
        ms: message.data.turboDurationMilliseconds
      , ticks: message.data.turboDurationTicks
      , factor: message.data.turboFactor
      , gameTick: message.gameTick
      });
    }
    return this.silence;
  };
  // --------------------------------------------
  // turbo request accepted, start
  this.turboStart = function(message) {
    if (message.data.color == _race.car.color)
      _race.car.startTurbo(message.gameTick);
    
    return this.silence;
  };
  // --------------------------------------------
  // turbo is finished
  this.turboEnd = function(message) {
    if (message.data.color == _race.car.color)
      _race.car.stopTurbo(message.gameTick);
    
    return this.silence;
  };
  // --------------------------------------------
  // lap finished
  // push limits a bit less
  this.lapFinished = function(message) {
    // register my own lap and adjust approximation factor
    if (message.data.car.color == _race.car.color) {
      _race.newLap();

      if (_race.qualifications) {
        if (_race.lastLap()) {
          _race.track.adjustLimitFactors(0);
        } else {
          _race.track.adjustLimitFactors(K.ADJUST_FACTOR);
        }
      }
    }
    // register lap done in my car / opponent
    _race.opponents[message.data.car.color].newLap(message.data.lapTime.millis);

    logger.info('Lap', message.data.lapTime.lap, 
                'finished by car', message.data.car.color, 
                'in', (message.data.lapTime.millis / 1000) + 's');
    return this.silence;
  };
  // --------------------------------------------
  // we have a winner
  this.finish = function(message) {
    logger.info('Car', message.data.color, 'finished at', _race.time / 1000);
    return this.silence;
  };
  // --------------------------------------------
  // disqualification
  this.dnf = function(message) {
    if (message.data.car.color == _race.car.color)
      _race.active = false;
    logger.warning('Car', message.data.car.color, 
                   'disqualified:', message.data.reason);
    return this.silence;
  }
  // --------------------------------------------
  // error
  this.error = function(message) {
    logger.error(message.data);
    return this.silence;
  } 
};
