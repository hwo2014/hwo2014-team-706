/* ********************************************
 * Turbo
 *
 * state and information about turbo
 * available from some tick
 * state change: book -> start -> stop
 * can be disposed if not in (start,stop)
 */

module.exports = function(params) {
  var _booked = false
    , _started = false
    , _used = false
    , _fromTick = params.gameTick
    ;

  this.ms = params.ms;  // duration in ms
  this.ticks = params.ticks;  // duration in ticks
  this.factor = params.factor;

  // a request to fire has been issued
  this.book = function() {
    _booked = true;
  };
  // the request has been approved
  this.start = function() {
    _started = true;
  };
  // turbo effect is finished
  this.stop = function() {
    _used = true;
  }
  // can we issue a request to fire ?
  this.isAvailable = function(tick) {
    return (!_booked &&
            !_used &&
            tick >= _fromTick);
  };
  // can we trash this because unused ?
  this.isDisposable = function() {
    return (!_booked || !_started);
  };
  // is turbo active ?
  this.inUse = function() {
    return (_started && !_used);
  };
  // is turbo finished ?
  this.isExhausted = function() {
    return (_used);
  };
};
