global._ = require('underscore');
global.K = require('./consts.js');

// sign of a number
global.sign = function(x) {
    return x ? x < 0 ? -1 : 1 : 0;
};
// between bounds
global.between = function(a,lo,hi) {
  return (a >= lo && a <= hi);
}
// tells if distance between numbers is less than some epsilon
global.closeTo = function(a, b, epsilon) {
  return Math.abs(a - b) < (epsilon || 0.01);
};
// linear interpolation
global.interpolate = function(x,x0,x1,y0,y1) {
  return (x - x0) / (x1 - x0) * (y1 - y0) + y0;
}
// array sum
_.sum = function(a) {
  return _.reduce(a, function(tot, v) {
    return tot + v;
  }, 0);
};

var net = require('net');
var JSONStream = require('JSONStream');
var Protocol = require('./protocol.js');
var Logger = require('./logger.js');

/* ********************************************
 * Main
 * 
 * Connect to server
 * create protocol handler on connect
 * pipe client data to json stream
 * parse messages on json stream events
 */

function main() {
  var serverHost = process.argv[2]
    , serverPort = process.argv[3]
    , botName = process.argv[4]
    , botKey  = process.argv[5]
    , trackName = process.argv[6]
    , trackPass = process.argv[7]
    , trackSlots = process.argv[8]
    , logger = new Logger({
          turbo: true
//         , grip: true
//       , status: false
//       , recv: true
//       , send: true
//        path: true
//         , lane: true
//         , brk: true
      })
    ;

  logger.debug("I'm", botName, "connecting to", serverHost + ":" + serverPort);
  logger.info(process.argv);

  var client = net.connect(serverPort, serverHost);

  client.on('connect', function() {
    // pipe json stream upon connection
    var jsonStream = client.pipe(JSONStream.parse());
    // create high-level protocol handler
    var protocol = new Protocol(logger, client);

    jsonStream.on('error', function() {
      logger.error('disconnected');
    });
    // handle json stream received data through high-level
    // protocol handler
    jsonStream.on('data', function(data) {
      logger.recv(data);
      // get protocol handler for received
      // message type
      var fun = protocol[data.msgType];
      if (fun === undefined) {
        logger.error('Unknown message', JSON.stringify(data));
        protocol.ping();
      } else {
        // process message and reply
        // with given protocol callback
        var reply = fun.call(protocol, data);
        reply.call(protocol);
      }
    });

    // start race
    if (trackPass)
      protocol.joinServer(botName, botKey, trackName, trackPass, trackSlots);
    else
      protocol.startServer(botName, botKey, trackName, trackSlots);
  });
};

main();