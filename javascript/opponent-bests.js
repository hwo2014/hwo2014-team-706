var OpponentBests = function(track) {

  var _pieceBests = [];
  for(p = 0; p < track.pieces; ++p) {
    _pieceBests.push({});
  }

  // substitutes new stats in best if avg speed is better
  this.evaluate = function(newStats) {
    newStats = newStats[0];
    var pieceIndex = newStats.pieces[0]
      , pathIndex  = '' + newStats.lanesIn[0] + newStats.lanesOut[0]
      , best       = _pieceBests[pieceIndex][pathIndex]
      ;

    if (best === undefined || (!best.locked && newStats.avgSpeed > best.avgSpeed)) {
      _pieceBests[pieceIndex][pathIndex] = newStats;
    }
  };
};

module.exports = OpponentBests;